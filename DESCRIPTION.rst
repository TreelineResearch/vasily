***********************************************************************************
                                    vasily
***********************************************************************************

                      knock knock knocking on ICMP's door
***********************************************************************************

Introduction:
	This script implements a mechanism for executing predefined commands based on a 
	received sequence of ICMP packets with predetermined sizes. The intended use 
	case is analogous to port knocking, with ICMP size sequences triggering iptables
	rule creation/deletion.

	The advantage of this is that the "knock" can be sent from any host capable of
	generating a ping of a specific size (which is most of them), and ICMP is
	often treated as network background noise, making the "knock" fairly subtle in
	terms of traffic signature.

Dependencies:
	- A recentish linux
	- Python 2.7
	- NetfilterQueue [optional >= 0.5.0]
	- Scapy 2 (and pcap dependencies)

Installation:
	1) Unpack like you do
	2) cd to that freshness
	3) $ sudo python setup.py install (or don't as root, you're a big boy figure it out)
	4) $ /etc/init.d/vasily start
	5) ping away like a towed sonar array

	This is known to work on linux, specifically Ubuntuish of recent vintage.
	There is currently no support for other platforms, but OSX may work with the scapy driver.
	You're on your own with YourFavoriteOS, send patches not whines.

Capture Setup:
	By default Vasily uses scapy and pcap to monitor incoming ICMP traffic, and 
	no configuration should be necessary for packet capture.

	Vasily can also use NetfilterQueue to ship packets from iptables to
	userland. To enable the queue, execute the following line, modifying 
	queue-num if necessary:

		# iptables -I INPUT -p icmp --icmp-type 8 -s 0/0 -j NFQUEUE --queue-num 1

Usage:
	Vasily accepts no arguments and looks for configuration parameters in
	"/etc/vasily.conf" or a configuration file defined in VASILY_CONF.

	Various configuration parameters and the triggerable actions are defined in vasily.conf, 
	refer to it for additional information.
	A simple shell script, pokeport.sh, is provided to open/close TCP ports using iptables, 
	let it serve as an example to all. 

	Example knock commands to send the reset magic packet and the 
	coded trigger sequence [1,2,3] from the client to the host example.com:

	Unixish:
		$ TARGET=example.com;ping -c 1 -s 777 $TARGET && ping -c 1 -s 1 $TARGET && ping -c 1 -s 2 $TARGET && ping -c 1 -s 3 $TARGET

	Windows:
		> ping -n 1 -l 777 example.com & ping -n 1 -l 4 example.com & ping -n 1 -l 5 example.com & ping -n 1 -l 6 example.com

	Accepts signals:
		SIGHUP  - reload configuration
		SIGUSR1 - dump status report to default log device

	Respects environment variables:
		VASILY_CONF=/some/config/dir/vasily.conf (default: /etc/vasily.conf)

Background:
	This is a fairly polished re-implementation of a perl script I prototyped
	around 10 years ago, and almost immediately lost forever due to a shitty ISP 
	and nonexistent backups. 
	I have no idea if it's an original idea or not, but I haven't seen anything 
	else do this with ICMP.

	It is named in reference to Vasily Borodin, who sent crafted pings for 
	Captain Ramius in "The Hunt for Red October".

Attribution:
	Vasily was written by K McGinley <kmm at rmlabs dot net> in late summer 2015.
	
===============================================================================