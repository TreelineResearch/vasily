#!/bin/bash
# pokeport.sh - trivial iptables helper script
# 
# Usage: pokeport.sh <open|close> <remote ip> <local port>

case "$1" in
	open)
		iptables -A INPUT -p tcp -s $2 --dport $3 -m state --state NEW,ESTABLISHED -j ACCEPT
		;;
	close)
		# smash all matching rules just in case
		while iptables -D INPUT -p tcp -s $2 --dport $3 -m state --state NEW,ESTABLISHED -j ACCEPT ; do false; done
		;;
esac

true