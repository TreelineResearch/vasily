#!/bin/bash
# pfpoke.sh - trivial pf script
# assumes existence of anchor 'vasily'
# Contributed by xenophyx
# Usage: pfpoke.sh <open|close> <remote ip> <local port>

case "$1" in
	open)
		echo "pass in quick proto tcp from $2 to any port $3" | pfctl -a vasily -f -
		;;
	close)
		# smash all matching rules just in case
		pfctl -a vasily -F rules
		;;
esac

true
