#!/usr/bin/env python
import os
import re
import sys
import time
import signal
import threading
from stat import *
from scapy.all import IP, Ether, sniff
import logging, logging.handlers
from subprocess import Popen,PIPE,STDOUT

__appname__ = "vasily"
__author__  = "K McGinley (kmm)"
__version__ = "0.5.1b"
__license__ = "BSD"
__date__    = "08/22/2015"

""" 
===================================================================================
                                  vasily.py
                      knock knock knocking on ICMP's door
===================================================================================
Introduction:
	This script implements a mechanism for executing predefined commands based on a 
	received sequence of ICMP packets with predetermined sizes. The intended use 
	case is analogous to port knocking, with ICMP size sequences triggering iptables
	rule creation/deletion.

	The advantage of this is that the "knock" can be sent from any host capable of
	generating a ping of a specific size (which is most of them), and ICMP is
	often treated as network background noise, making the "knock" fairly subtle in
	terms of traffic signature.

Dependencies:
	- A recentish linux
	- Python 2.7
	- NetfilterQueue [optional >= 0.5.0]
	- Scapy 2 (and pcap dependencies)

Installation:
	1) Unpack like you do
	2) cd to that freshness
	3) $ sudo python setup.py install (or don't as root, you're a big boy figure it out)
	4) $ /etc/init.d/vasily start
	5) ping away like a towed sonar array

	This is known to work on linux, specifically Ubuntuish of recent vintage.
	There is currently no support for other platforms, but OSX may work with the scapy driver.
	You're on your own with YourFavoriteOS, send patches not whines.

Capture Setup:
	By default Vasily uses scapy and pcap to monitor incoming ICMP traffic, and 
	no configuration should be necessary for packet capture.

	Vasily can also use NetfilterQueue to ship packets from iptables to
	userland. To enable the queue, execute the following line, modifying 
	queue-num if necessary:

		# iptables -I INPUT -p icmp --icmp-type 8 -s 0/0 -j NFQUEUE --queue-num 1

Usage:
	Vasily accepts no arguments and looks for configuration parameters in
	"/etc/vasily.conf" or a configuration file defined in VASILY_CONF.

	Various configuration parameters and the triggerable actions are defined in vasily.conf, 
	refer to it for additional information.
	A simple shell script, pokeport.sh, is provided to open/close TCP ports using iptables, 
	let it serve as an example to all. 

	Example knock commands to send the reset magic packet and the 
	coded trigger sequence [1,2,3] from the client to the host example.com:

	Unixish:
		$ TARGET=example.com;ping -c 1 -s 777 $TARGET && ping -c 1 -s 1 $TARGET && ping -c 1 -s 2 $TARGET && ping -c 1 -s 3 $TARGET

	Windows:
		> ping -n 1 -l 777 example.com & ping -n 1 -l 4 example.com & ping -n 1 -l 5 example.com & ping -n 1 -l 6 example.com

	Accepts signals:
		SIGHUP  - reload configuration
		SIGUSR1 - dump status report to default log device

	Respects environment variables:
		VASILY_CONF=/some/config/dir/vasily.conf (default: /etc/vasily.conf)

Background:
	This is a fairly polished re-implementation of a perl script I prototyped
	around 10 years ago, and almost immediately lost forever due to a shitty ISP 
	and nonexistent backups. 
	I have no idea if it's an original idea or not, but I haven't seen anything 
	else do this with ICMP.

	It is named in reference to Vasily Borodin, who sent crafted pings for 
	Captain Ramius in "The Hunt for Red October".

Attribution:
	Vasily was written by K McGinley <kmm at rmlabs dot net> in late summer 2015.

===============================================================================

Copyright (c) 2015, K McGinley
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

CONFIG_FILE = os.environ["VASILY_CONF"] if "VASILY_CONF" in os.environ else "/etc/vasily.conf"

class State:
	IDLE, GETSEQ, OPEN = range(3)

class Client:
	def __init__(self, ip = "0.0.0.0"):
		self.ip = ip
		self.state = State.IDLE
		self.seqbuf = []
		self.timer = 0
		self.active_triggers = []
		self.session_start = {}

class ICMPKnock:
	def __init__(self, config):
		self.clients = {}
		
		self.logger = logging.getLogger()
		self.load_config(config)
		
		self.logger.info("ICMP knocker Vasily %s has the conn." % __version__)
		self.logger.debug("\"Re-verify our range to target... one ping only.\"")
		
		self.tkthread = threading.Thread(target = self.timekeeper)
		self.tkthread.setDaemon(True)
		self.tkthread.start()

	def load_config(self, config):
		self.logger = logging.getLogger() # replace the default logger
		self.config = config["general"]
		self.triggers = config["triggers"]

		loglevel = self.config["loglevel"] if "loglevel" in self.config else logging.DEBUG
		self.logger.setLevel(loglevel)
		
		if "syslog_address" in self.config:
			syslog_address = self.config["syslog_address"]
			h = logging.handlers.SysLogHandler(address=syslog_address, facility=logging.handlers.SysLogHandler.LOG_USER)
		else:
			h = logging.handlers.SysLogHandler(address="/dev/log", facility=logging.handlers.SysLogHandler.LOG_USER)

		h.setFormatter(logging.Formatter('vasily: %(message)s'))
		self.logger.addHandler(h)

		self.logger.info("logging to syslog at %s" % str(h.address))
		self.logger.info("configuration loaded: %i triggers, sequence window %i sec, reset packet length %i bytes" % (len(self.triggers), self.config["window"], self.config["reset"]))

		for t in self.triggers:
			self.logger.info("loaded trigger: %s" % t["name"])
	
	def get_status(self):
		self.logger.info("*******     vasily status     *******")
		for ip, client in self.clients.iteritems():
			self.logger.info(">>>>>>> %s" % ip)
			self.logger.info("client state: %s" % client.state)
			self.logger.info("active triggers: %s" % ", ".join(client.active_triggers))
			self.logger.info("- open sessions -")
			for tname, ss in client.session_start.iteritems():
				self.logger.info("  -> %s (%i)" % (tname, ss))


	def reset(self, src_ip, state = State.GETSEQ):
		if src_ip not in self.clients:
			self.clients[src_ip] = Client(src_ip)
		now = time.time()
		self.clients[src_ip].timer = now
		self.clients[src_ip].seqbuf = []
		self.clients[src_ip].state = state

	def session(self, src_ip):
		return self.clients[src_ip]

	def run_action(self, client, trigger, action):
		action = action.replace("{ip}", client.ip)
		action = action.replace("{name}", trigger["name"])
		action = action.replace("{sequence}", "'[%s]'" % ",".join([str(x) for x in client.seqbuf]))
		self.logger.info("executing command: '%s'" % action)
		try:
			fname = re.compile("\s+").split(action)[0]
			mode = os.stat(fname).st_mode
			if mode & 02:
				self.logger.error("skipping execution of world-writable action script '%s' (mode: %s)" % (fname, oct(mode & 0777)))
				return None
			proc = Popen(action, shell=True, stdout=PIPE, stderr=STDOUT, close_fds=True)
			errval = proc.wait()
			self.logger.debug("process output (%i): %s" % (errval, proc.stdout.read()))
			if errval != 0:
				self.logger.error("action exec failed with shell error code: %i" % errval)
			else:
				self.logger.debug("action executed successfully")
		except:
			(etype, obj, tb) = sys.exc_info()[0:3]
			self.logger.error("execution failed with python exception:%s on line %i %s" % (etype, tb.tb_lineno, obj))

	def handle_pkt(self, pkt):
		self.logger.debug("packet handler called")
		if not self.config["driver"] == "scapy":
			pkt.accept()
			dpkt = IP(pkt.get_payload())
		else:
			dpkt = IP(str(pkt.payload))

		plen = dpkt.len - 28
		self.logger.debug("packet: %s" % dpkt.__repr__())
		src_ip = dpkt.src
		

		if plen == self.config["reset"]:
			self.logger.debug("reset packet received")
			self.reset(src_ip)
		elif "extend" in self.config and plen == self.config["extend"]:
			self.logger.debug("extend session packet received")
			for s in self.session(src_ip).session_start:
				self.session(src_ip).session_start[s] = time.time()
		elif "terminate" in self.config and plen == self.config["terminate"]:
			self.logger.debug("terminate session packet received")
			for s in self.session(src_ip).session_start:
				self.session(src_ip).session_start[s] = 0
		elif (self.session(src_ip).timer - time.time()) < self.config["window"] and self.session(src_ip).state == State.GETSEQ:
			self.logger.debug("appending sequence element: %s <- %s" % (self.session(src_ip).seqbuf, plen))
			self.session(src_ip).seqbuf.append(plen)
		else:
			self.logger.info("sequence window expired")
			self.reset(src_ip, State.IDLE)

	def timekeeper(self):
		while True:
			if int(time.time() % 300) == 0:
				self.logger.info("service alive: %i clients known, %i clients active" % (len(self.clients), 
					                                                                 len([c for c in self.clients if len(self.clients[c].active_triggers) > 0])))
				diag_en = True
			else:
				diag_en = False

			for ip, client in self.clients.iteritems():
				if diag_en:
					self.logger.debug("%s active triggers: %s" % (ip, client.active_triggers))
				for t in self.triggers:
					if diag_en and t["name"] in client.session_start:
						self.logger.debug("[%s] %s session time remaining: %0.1f seconds" % (ip, t["name"], t["timeout"] - (time.time() - client.session_start[t["name"]])))
					if client.state == State.GETSEQ and (time.time() > client.timer + self.config["window"]) and len(client.seqbuf) > 0:
						if t["sequence"] == client.seqbuf:
							if t["name"] not in client.active_triggers:
								self.logger.info("[%s] trigger open: '%s'" % (ip, t["name"]))
								self.logger.debug("[%s] executing open action: '%s'" % (ip, t["openaction"]))
								
								self.run_action(client, t, t["openaction"])
								
								client.active_triggers.append(t["name"])
								client.session_start[t["name"]] = time.time()
								client.state = State.OPEN
							else:
								self.logger.info("[%s] trigger '%s' timeout reupped" % (ip, t["name"]))
								client.session_start[t["name"]] = time.time()
								client.state = State.OPEN
					elif  client.state == State.OPEN and t["name"] in client.active_triggers and (time.time() - client.session_start[t["name"]]) > t["timeout"]:
						self.logger.info("[%s] trigger time out: %s" % (ip, t["name"]))
						self.logger.debug("[%s] executing close action: '%s'" % (ip, t["closeaction"]))
						
						self.run_action(client, t, t["closeaction"])

						client.active_triggers = [name for name in client.active_triggers if name != t["name"]]
						client.session_start = {name:timeout for (name,timeout) in client.session_start.iteritems() if t["name"] != name}
						if len(client.active_triggers) == 0:
							self.logger.debug("[%s] client has no active triggers, going idle" % ip)
							client.state = State.IDLE

			sys.stdout.flush()
			time.sleep(1)

# Wrapper for standalone invocation from module init
class Standalone:
	def __init__(self):
		logging.basicConfig(level=logging.DEBUG, format='vasily: %(message)s')
		self.knock = None
		conf = self.read_config(CONFIG_FILE)
		self.knock = ICMPKnock(conf)
		
		signal.signal(signal.SIGHUP, self.handle_signal)
		signal.signal(signal.SIGUSR1, self.handle_signal)

		if self.knock.config["driver"] == "scapy":
			self.knock.logger.info("using scapy driver")
			try:
				sniff(filter="icmp[icmptype] == 8", prn=self.knock.handle_pkt)
			except KeyboardInterrupt:
				logging.info("keyboard interrupt received, exiting.")
				sys.exit()				
		else:
			self.knock.logger.info("using nfqueue driver")
			from netfilterqueue import NetfilterQueue
			nfq = NetfilterQueue()
			nfq.bind(conf["general"]["nfqueue"], self.knock.handle_pkt)

			try:
				while True:   
					nfq.run() # this is only wrapped in a loop because SIGHUP appears to make it terminate and i can't be arsed to figure out why
			except KeyboardInterrupt:
				logging.info("keyboard interrupt received, exiting.")
				sys.exit()

	def read_config(self, filename):
		conf = {}
		try:
			execfile(filename, conf)
		except IOError:
			logging.warning("unable to open config file")

		if not "general" in conf:
			logging.warning("no general configuration block found, using defaults")
			conf["general"] = {   # default core parameters
				"reset":777,      # reset/magic packet length
				"window":1        # how long to watch for a given coded sequence of ICMP packets after reset packet received, in seconds
			}
		if not "triggers" in conf:
			logging.warning("no trigger configuration block found, using defaults")
			conf["triggers"] = [       # a default trigger definition
				{
				  "name":"rtfm",       # arbitrary string descriptor, should be unique
				  "sequence":[1,2,3],  # the coded sequence of ICMP echo packet sizes to listen for
			      "openaction":"echo 'this would be much more useful with a config file.'",    # command to execute when the coded sequence is received
			      "closeaction":"echo 'this would be much more useful with a config file.'",   # command to execute when the session expires
			      "timeout":60         # session lifetime in seconds
			     }
			]
		if not "nfqueue" in conf["general"]:
			conf["general"]["nfqueue"] = 1 # default nfqueue

		return conf

	def handle_signal(self, signum, frame):
		global knock
		if signum == signal.SIGHUP:
			logging.info("reloading configuration")
			conf = self.read_config(CONFIG_FILE)
			self.knock.load_config(conf)
		elif signum == signal.SIGUSR1:
			self.knock.get_status()

if __name__ == "__main__":
	Standalone()